import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todoList: []
  },
  mutations: {
    updateTodoList(state, val) {
      state.todoList = val;
    },
    addToTodoList(state, val) {
      state.todoList.push(val);
    },
    updateTodoItem(state, val) {
      const foundedIndexForChangedTodoItem = state.todoList.findIndex(
        item => item.id === val.id
      );
      state.todoList[foundedIndexForChangedTodoItem] = val;
    }
  },
  actions: {
    getTodoList({ commit }) {
      axios
        .get("https://jsonplaceholder.typicode.com/todos")
        .then(response => {
          commit("updateTodoList", response.data);
        })
        .catch(err => {
          console.log(err);
        });
    },
    addToTodoList({ commit }, todo) {
      axios
        .post("https://jsonplaceholder.typicode.com/todos")
        .then(response => {
          commit("addToTodoList", { ...todo, ...response.data });
        })
        .catch(err => {
          console.log(err);
        });
    },
    changeTodo({ commit }, todo) {
      axios
        .patch(`https://jsonplaceholder.typicode.com/todos/${todo.id}`, {
          data: todo
        })
        .then(response => {
          commit("updateTodoItem", response.data);
        })
        .catch(err => {
          console.log(err);
        });
    }
  },
  modules: {}
});
